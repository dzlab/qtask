import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Hashtable;
import java.util.Map;
import java.util.regex.Pattern;


public class mfe {

	public static int BIGSIZE = 128 * 1024 * 1024;
	public static int SIZE = 1024 * 1024;
	
	public static Pattern REGEX_PATTERN = Pattern.compile(" ");
			
	public static void main(String[] args) {
		if(args.length==0)
			throw new IllegalArgumentException("Should provide a path to the file to read");
		String name = args[0]; //"pg4300.txt";	
		long t1, t2;
		String result = null;
		
		t1 = System.currentTimeMillis();
		result = readAsChars(name);
		t2 = System.currentTimeMillis();
		System.out.println("'"+result+"' found in "+(t2-t1)+"ms");
		
		t1 = System.currentTimeMillis();
		result = readAsBytes(name);
		t2 = System.currentTimeMillis();
		System.out.println("'"+result+"' found in "+(t2-t1)+"ms");
	}

	/**consider the file as text file and perform byte-to-char parsing*/
	public static String readAsChars(String name) {
		Hashtable<String, Integer> map = new Hashtable<String, Integer>(SIZE);
		String result = null; int max = 0;
		String word;		
		Integer count;
		BufferedReader br = null;
		try {
			char[] cbuf = new char[SIZE]; 
			br = new BufferedReader(new FileReader(name), BIGSIZE);
			while (br.read(cbuf) != -1) {				
				StringBuilder sb = new StringBuilder();
				for(char c: cbuf) {
					if(c==' ' || c=='\n') {
						word = sb.toString();
						count = map.get(word);	
						count = (count==null? 0: count+1);
						map.put(word, count);	
						/*if(count>max) {
							result = word;
							max = count;
						}*/
						sb = new StringBuilder();
					}else {
						sb.append(c);
					}
				}				
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null) br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}	
		result = findStringWithMaxOccurrence(map);
		return result;
	}
	
	/**consider the file as binary file and avoid byte-to-char parsing*/
	public static String readAsBytes(String name) {
		Hashtable<String, Integer> map = new Hashtable<String, Integer>(SIZE);
		String word, result = null; int max = 0;
		Integer count;
		StringBuilder sb = new StringBuilder();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream( name );
			FileChannel ch = fis.getChannel( );
			ByteBuffer bb = ByteBuffer.allocateDirect( BIGSIZE );
			byte[] barray = new byte[SIZE];			
			int nRead, nGet;
			char c;
			while ( (nRead=ch.read( bb )) != -1 ) {
			    if ( nRead == 0 )
			        continue;
			    bb.position( 0 );
			    bb.limit( nRead );
			    while( bb.hasRemaining( ) ) {
			        nGet = Math.min( bb.remaining( ), SIZE );
			        bb.get( barray, 0, nGet );
			        for ( int i=0; i<nGet; i++ ) {
			            c = (char) barray[i];
			            if(c==' ' || c=='\n') {
							word = sb.toString();
							count = map.get(word);	
							count = (count==null? 0: count+1);
							map.put(word, count);	
							/*if(count>max) {
								result = word;
								max = count;
							}*/
							sb = new StringBuilder();
						}else {
							sb.append(c);
						}
			        }
			    }
			    bb.clear( );
			}
		}catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null) fis.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}	
		result = findStringWithMaxOccurrence(map);
		return result;
	}
	
	public static String findStringWithMaxOccurrence(Map<String, Integer> map) {
		String result = null;
		Integer count, max=0;
		for(Map.Entry<String, Integer> e: map.entrySet()) {
			count = e.getValue();
			if(count>max) {
				result = e.getKey();
				max = count;
			}
		}	
		return result;
	}
}
